/*******************************************************************************
 * This is a simple program which connects to a server at the IP specified on
 * the command-line, reads the current date/time from it, and prints out the
 * data received.
*******************************************************************************/
#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>

#define MAXLINE         (4096)

int
main(int argc, char **argv)
{
    char recvline[MAXLINE + 1]  = { 0 };
    int n       = 0;
    int sockfd  = 0;
    struct sockaddr_in servaddr; 

    /* Make sure we have an argument to process as an IP address */
    if (2 != argc)
    {
        printf("Usage: %s <IP address>\n", argv[0]);
        exit(-1);
    }

    /* Create a TCP socket */
    if (0 > (sockfd = socket(AF_INET, SOCK_STREAM, 0)))
    {
        perror("Failed to create socket\n");
        exit(-1);
    }

    memset(&servaddr, 0, sizeof(servaddr));
    servaddr.sin_family = AF_INET;
    servaddr.sin_port = htons(13);

    /* Convert the string IP to an integer IP */
    if (0 >= inet_pton(AF_INET, argv[1], &servaddr.sin_addr))
    {
        printf("Failed to convert %s to an integer IP\n", argv[1]);
        exit(-1);
    }

    /* Connect to a server at the IP given on the command line */
    if (0 > connect(sockfd, (struct sockaddr *) &servaddr, sizeof(servaddr)))
    {
        printf("Failed to connect to a server at %s\n", argv[1]);
        exit(-1);
    }

    /* Read up to MAXLINE bytes at a time until no data is received */
    while (0 < (n = read(sockfd, recvline, MAXLINE)))
    {
        recvline[n] = 0;

        /* Print out whatever we read from the server */
        if (EOF == fputs(recvline, stdout))
        {
            printf("fputs() error\n");
            exit(-1);
        }
    }

    /* Indicate if no data was received */
    if (0 < n)
    {
        printf("Read error\n");
        exit(-1);
    }

    exit(0);
}
