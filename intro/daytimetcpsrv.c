/*******************************************************************************
 * This is a simple program which establishes a server which, when connected to,
 * will respond with the current date.
*******************************************************************************/
#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <time.h>

#define MAXLINE         (4096)
#define LISTENQ         (1024)

int
main(int argc, char **argv)
{
    char sendline[MAXLINE + 1]  = { 0 };
    int listenfd    = 0;
    int connfd      = 0;
    struct sockaddr_in servaddr; 
    time_t ticks    = 0;

    /* Create a TCP socket */
    if (0 > (listenfd = socket(AF_INET, SOCK_STREAM, 0)))
    {
        perror("Failed to create socket\n");
        exit(-1);
    }

    memset(&servaddr, 0, sizeof(servaddr));
    servaddr.sin_family = AF_INET;
    servaddr.sin_addr.s_addr = htonl(INADDR_ANY);
    servaddr.sin_port = htons(13);

    /* Bind the socket to the port we want to use */
    if (0 > bind(listenfd, (struct sockaddr *) &servaddr, sizeof(servaddr)))
    {
        printf("Failed to bind to port. Try running as root.\n");
        exit(-1);
    }

    /* Connect to a server at the IP given on the command line */
    if (0 > listen(listenfd, LISTENQ))
    {
        printf("Failed to establish the listen queue\n");
        exit(-1);
    }

    for (;;)
    {
        if (0 > (connfd = accept(listenfd, (struct sockaddr *) NULL, NULL)))
        {
            printf("Failed to accept client connection\n");
            exit(-1);
        }

        ticks = time(NULL);
        snprintf(sendline, sizeof(sendline), "%.24s\r\n", ctime(&ticks));

        if (0 > write(connfd, sendline, strlen(sendline)))
        {
            printf("Failed to send data to client\n");
            exit(-1);
        }

        if (0 > close(connfd))
        {
            printf("Failed to close connected clients file descriptor\n");
            exit(-1);
        }
    }

    exit(0);
}
